import talib
from numpy import genfromtxt

my_data = genfromtxt('./datasets/klines_historical_btcusdt_15minute.csv', delimiter=',')
closings = my_data[:, 4]
rsi = talib.RSI(closings)

print(rsi)
