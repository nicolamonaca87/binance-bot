import numpy
import talib

from binance.enums import *
from configs import rsi_config


class Indicator:
    def __init__(self):
        self.in_position = False
        self.closes = []

    def perform_rsi(self, bot, json_message):
        candle = json_message['k']
        is_candle_closed = candle['x']
        close = candle['c']

        if is_candle_closed:
            self.closes.append(float(close))
            print("Closes: {}".format(self.closes))

            if len(self.closes) > rsi_config.RSI_PERIOD:
                np_closes = numpy.array(self.closes)
                rsi_indicator = talib.RSI(np_closes, rsi_config.RSI_PERIOD)
                last_rsi = rsi_indicator[-1]
                print("The current RSI is {}".format(last_rsi))

                if last_rsi < rsi_config.RSI_OVERSOLD:
                    if self.in_position:
                        print("> It is oversold, but you already own it, nothing to do.")
                    else:
                        print("> Oversold! BUY! BUY! BUY!")
                        # Buy logic
                        order_succeeded = bot.order(SIDE_BUY, bot.trade_symbol)
                        if order_succeeded:
                            print("Order succeeded")
                            self.in_position = True

                if last_rsi > rsi_config.RSI_OVERBOUGHT:
                    if self.in_position:
                        print("> Overbought! SELL! SELL! SELL!")
                        # Sell logic
                        order_succeeded = bot.order(SIDE_SELL, bot.trade_symbol)
                        if order_succeeded:
                            print("Order succeeded")
                            self.in_position = False
                    else:
                        print("> It is overbought, but we don't own any. Nothing to do.")
