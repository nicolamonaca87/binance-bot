from flask import Flask, render_template, request, redirect, flash, jsonify
from flask_socketio import SocketIO
from flask_cors import CORS
from helpers.binance_utilities import BinanceUtilities
from bot import Bot
from configs.config import Config
import threading


app = Flask(__name__)
CORS(app)
app.secret_key = b'fnuewfuwveoucwfwerg73efg9f79g23qflp3fo3fjeif'
socketio = SocketIO(app, async_mode=None, cors_allowed_origins="*")
trading_symbols = []

thread = threading.Thread()
utilities = BinanceUtilities()


@app.route('/')
def index():
    balance_info = Bot.client.get_account()['balances']
    exchange_info = Bot.client.get_exchange_info()

    balances = [balance_dict for balance_dict in balance_info if float(balance_dict['free']) > 0]
    symbols = exchange_info['symbols']

    socketio.emit("CHART_HISTORICAL_DATA", )

    return render_template('index.html', balances=balances, symbols=symbols)


@app.route('/trade', methods=['POST'])
def trade():
    symbol = request.form['symbol']
    quantity = request.form['quantity']
    indicators = []

    # Get the selected indicators from the web page (RSI, etc...)
    for parameter in request.form:
        if parameter.startswith("indicators"):
            indicators.append(request.form[parameter])

    # Check if I'm already trading the current symbol
    if symbol not in trading_symbols:
        try:
            trading_symbols.append(symbol)

            global thread
            if not thread.is_alive():
                thread = Bot(symbol=symbol, quantity=quantity, indicators=indicators, socketio=socketio)
                thread.start()
                flash("Trade Running: {} {} with {}".format(quantity, symbol, indicators), "INFO")
        except Exception as e:
            flash("An error occurred: {}".format(str(e)), "ERROR")
    else:
        socketio.emit("CANNOT_TRADE_SYMBOL",
                      {'message': 'Cannot trade {} since another trade on the same symbol is ongoing.'.format(symbol)},
                      namespace='/trade',
                      broadcast=True)
        flash("Cannot trade {} since another trade on the same symbol is ongoing.".format(symbol), "ERROR")

    return redirect('/')


@app.route('/details')
def details():
    return str(utilities.my_trades(symbol="BTCUSDT"))


@app.route('/history/<symbol>/<timeframe>')
def history(symbol, timeframe):
    candles = utilities.get_historical_data(symbol=symbol,
                                            timeframe=timeframe,
                                            start="1 Aug, 2021",
                                            end=None,
                                            data_presentation_mode='raw'
                                            )

    processed_candles = [
        {
            "time": int(candle[0] / 1000),
            "open": float(candle[1]),
            "high": float(candle[2]),
            "low": float(candle[3]),
            "close": float(candle[4])
        } for candle in candles]

    return jsonify(processed_candles)


if __name__ == '__main__':
    socketio.run(app, debug=Config.DEBUG_MODE)
