$(document).ready(function() {
    // Connect to the socket server
    let socket = io.connect('http://' + document.domain + ':' + location.port + '/trade')
    let received_messages = []

    socket.on('NEW_MESSAGE', function(msg) {
        received_messages.push(msg.message)
        let message_string = ''

        // Build the HTML elements holding the response
        for(i = 0; i < received_messages.length; i++) {
            message_string = message_string + '<p>' + received_messages[i].toString() + '</p>'
        }

        // Append the new message in the HTML div
        // $('#notifications-box').html(message_string)
    });

    socket.on('CANNOT_TRADE_SYMBOL', function(msg) {
        $('#notifications-box').html('<p>' + msg.message + '</p>')
        console.log(msg.message)
    });
});