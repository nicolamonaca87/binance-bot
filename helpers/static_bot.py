import json
import websocket
from binance.client import Client
from configs.config import Config
from indicators.indicators import *


class Bot:
    config = Config()
    client = Client(config.current_key, config.current_secret, tld='com', testnet=config.USING_TESTNET)

    def __init__(self, symbol, quantity, indicators):
        self.indicator = Indicator()

        # Set trading parameters
        self.TRADE_SYMBOL = symbol
        self.desired_quantity = quantity  # This amount will automatically be adjusted, if the value is below 10$
        self.indicators = indicators

        self.trading_info = None
        self.in_position = False

        # Initialize
        self.socket = self.config.current_socket.format(self.TRADE_SYMBOL.lower() + "@kline_" + self.config.TIMEFRAME)
        print("Connecting to: {}".format(self.socket))
        print()

        # Get the trading info
        self.trading_info = self.client.get_symbol_info(self.TRADE_SYMBOL)
        # print(self.trading_info)

        self.ws = websocket.WebSocketApp(self.socket,
                                         on_open=self.on_open,
                                         on_close=self.on_close,
                                         on_message=self.on_message)

    def run(self):
        self.ws.run_forever()

    @staticmethod
    def on_open(ws):
        print('Opened connection')

    @staticmethod
    def on_close(ws):
        print('Closed connection')

    def on_message(self, ws, message):
        print('Received message')
        json_message = json.loads(message)
        # pprint.pprint(json_message)
        # score = 0

        for ind in self.indicators:
            if ind.upper() == 'RSI':
                # score += indicator.perform_rsi(json_message)  # Invoke perform_rsi()
                self.indicator.perform_rsi(self, json_message)  # Invoke perform_rsi()
            # More indicators to follow
                # ...

        # if score > self.config.SCORE_THRESHOLD:
            # pass
            # order

    # Set the actual tradable quantity
    def calculate_quantity(self):
        minimum_allowed_quantity = float(self.trading_info['filters'][3]['minNotional'])
        final_quantity = self.desired_quantity
        allowed_quantity = None
        lot_size = float(self.trading_info['filters'][2]['stepSize'])

        # @TODO
        # In order to pass the lot size, the following must be true for quantity/icebergQty:
        # quantity >= minQty
        # quantity <= maxQty
        # (quantity - minQty) % stepSize == 0

        # Get latest price
        price = float(self.client.get_ticker(symbol=self.TRADE_SYMBOL)['lastPrice'])

        try:
            decimals = str(format(lot_size, '.6f')).split('.')[1]
            if int(decimals) > 0:  # If we have decimals
                decimals = len(decimals)
                allowed_quantity = round(minimum_allowed_quantity / price + lot_size, decimals)
            else:  # Otherwise, if we have no decimals
                decimals = 0
                allowed_quantity = int(round(minimum_allowed_quantity / price + lot_size, decimals))
        except IndexError:  # If we had an integer lot_size number in the first place
            decimals = 0
            allowed_quantity = int(round(minimum_allowed_quantity / price + lot_size, decimals))

        if float(self.desired_quantity) < float(allowed_quantity):
            final_quantity = allowed_quantity

        if self.config.DEBUG_MODE:
            print()
            print("Debug Info for calculate_quantity() method:")
            print("* * * * * * * * * * * * * * * * * * * * * *")
            print("* minimum_allowed_quantity: " + str(minimum_allowed_quantity))
            print("* allowed_quantity: " + str(allowed_quantity))
            print("* final_quantity: " + str(final_quantity))
            print("* lot_size: " + str(lot_size))
            print("* price: " + str(price))
            print("* decimals: " + str(decimals))
            print("* * * * * * * * * * * * * * * * * * * * * *")
            print()

        return float(final_quantity), float(price)

    def order(self, side, symbol, order_type=ORDER_TYPE_MARKET):
        try:
            print("Sending order")

            order_quantity, price = self.calculate_quantity()

            print("Trading {} {} at {} {} for {} {}".format(str(order_quantity),
                                                            str(self.trading_info['baseAsset']),
                                                            str(price),
                                                            str(self.trading_info['quoteAsset']),
                                                            str(round(order_quantity * price, 2)),
                                                            str(self.trading_info['quoteAsset'])))

            if self.config.DEBUG_MODE:
                print()
                print("Debug Info for order() method:")
                print("* * * * * * * * * * * * * * *")
                print("* symbol: " + symbol)
                print("* order_quantity: " + str(order_quantity))
                print("* price: " + str(price))
                print("* trading_info['baseAsset']: " + self.trading_info['baseAsset'])
                print("* trading_info['quoteAsset']: " + self.trading_info['quoteAsset'])
                print("* side: " + side)
                print("* order_type: " + order_type)
                print("* * * * * * * * * * * * * * *")
                print()

            if self.config.TEST_ORDER_MODE:
                order = self.client.create_test_order(symbol="RYUR", side=side, type=order_type, quantity=order_quantity)
            else:
                order = self.client.create_order(symbol=symbol, side=side, type=order_type, quantity=order_quantity)

            print(order)

        except Exception as e:
            print("An error occurred: {}".format(e))
            return False
        return True


if __name__ == "__main__":
    bot = Bot(symbol='BTCUSDT', quantity='0.002', indicators=['RSI'])

    # Test a one-shot order
    # new_order = bot.order(side=SIDE_BUY, symbol=bot.TRADE_SYMBOL)
    # print(new_order)
    # exit()

    bot.run()
