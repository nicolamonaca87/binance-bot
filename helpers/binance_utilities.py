from configs.config import Config
from binance.client import Client
import csv


class BinanceUtilities:
    def __init__(self):
        self.config = Config()
        self.client = Client(self.config.current_key, self.config.current_secret, tld='com',
                             testnet=self.config.USING_TESTNET)

        self.klines_dict = {
            '1MINUTE': self.client.KLINE_INTERVAL_1MINUTE,
            '3MINUTE': self.client.KLINE_INTERVAL_3MINUTE,
            '5MINUTE': self.client.KLINE_INTERVAL_5MINUTE,
            '15MINUTE': self.client.KLINE_INTERVAL_15MINUTE,
            '30MINUTE': self.client.KLINE_INTERVAL_30MINUTE,
            '1HOUR': self.client.KLINE_INTERVAL_1HOUR,
            '2HOUR': self.client.KLINE_INTERVAL_2HOUR,
            '4HOUR': self.client.KLINE_INTERVAL_4HOUR,
            '6HOUR': self.client.KLINE_INTERVAL_6HOUR,
            '8HOUR': self.client.KLINE_INTERVAL_8HOUR,
            '12HOUR': self.client.KLINE_INTERVAL_12HOUR,
            '1DAY': self.client.KLINE_INTERVAL_1DAY,
            '3DAY': self.client.KLINE_INTERVAL_3DAY,
            '1WEEK': self.client.KLINE_INTERVAL_1WEEK,
            '1MONTH': self.client.KLINE_INTERVAL_1MONTH,
        }

    def my_trades(self, symbol):
        return self.client.get_my_trades(symbol=symbol)

    def all_tickers(self):
        return self.client.get_all_tickers()

    def get_latest_data(self, symbol, timeframe, data_presentation_mode='csv'):
        if timeframe in self.klines_dict:
            candles = self.client.get_klines(symbol=symbol, interval=self.klines_dict[timeframe])
            if data_presentation_mode == 'csv':
                self.save_csv_data(candles, symbol, timeframe, type='latest')
            elif data_presentation_mode == 'console':
                for candle in candles:
                    print(candle)
            elif data_presentation_mode == 'raw':
                return candles
            else:
                return Exception("Invalid presentation mode: it can be either 'csv', 'console' or 'raw'")
        else:
            return Exception('Invalid timeframe')

    def get_historical_data(self, symbol, timeframe, start=None, end=None, data_presentation_mode='csv'):
        if timeframe in self.klines_dict:
            candles = self.client.get_historical_klines(symbol=symbol, interval=self.klines_dict[timeframe], start_str=start, end_str=end)
            if data_presentation_mode == 'csv':
                self.save_csv_data(candles, symbol, timeframe, type='historical')
            elif data_presentation_mode == 'console':
                for candle in candles:
                    print(candle)
            elif data_presentation_mode == 'raw':
                return candles
            else:
                return Exception("Invalid presentation mode: it can be either 'csv', 'console' or 'raw'")
        else:
            return Exception('Invalid timeframe')

    @staticmethod
    def save_csv_data(candles, symbol, timeframe, type='latest'):
        if type == 'latest':
            csv_file = open('datasets/klines_latest_{}_{}.csv'.format(symbol.lower(), timeframe.lower()), 'w', newline='')
        else:
            csv_file = open('datasets/klines_historical_{}_{}.csv'.format(symbol.lower(), timeframe.lower()), 'w', newline='')

        writer = csv.writer(csv_file, delimiter=',')

        for candle in candles:
            writer.writerow(candle)

        print("{} {} candles saved for {} on {} timeframe".format(len(candles), type, symbol, timeframe))

        return csv_file


if __name__ == '__main__':
    utilities = BinanceUtilities()
    trades = utilities.my_trades(symbol="BTCUSDT")
    utilities.get_latest_data(symbol="BTCUSDT", timeframe="15MINUTE", data_presentation_mode='csv')
    utilities.get_historical_data(symbol="BTCUSDT", timeframe="15MINUTE", start="1 Jan, 2012", end=None, data_presentation_mode='csv')
    # print(trades)

    # prices = utilities.all_tickers()
    # for price in prices:
    #    print(price)
